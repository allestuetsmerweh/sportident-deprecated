/* global si */
/* exported si */

// eslint-disable-next-line no-use-before-define
if (typeof si !== 'undefined') { console.warn('si variable already existed. Overwriting...'); }

var si = {};


si.timeoutResolvePromise = (value, timeout = 1) =>
    new Promise((resolve, _reject) =>
        setTimeout(() => resolve(value), timeout));
si.timeoutRejectPromise = (reason, timeout) =>
    new Promise((resolve, _reject) =>
        setTimeout(() => resolve(reason), timeout));


// ###### proto ######

si.proto = {
    // Protocol characters
    STX: 0x02,
    ETX: 0x03,
    ACK: 0x06, // when sent to BSx3..6, causes beep until SI-card taken out
    NAK: 0x15,
    DLE: 0x10,
    WAKEUP: 0xFF,

    // Basic protocol commands, currently unused
    basicCmd: {
        SET_CARDNO: 0x30,
        GET_SI5: 0x31, // read out SI-card 5 data
        SI5_WRITE: 0x43, // 02 43 (block: 0x30 to 0x37) (16 bytes) 03
        SI5_DET: 0x46, // SI-card 5 inserted (46 49) or removed (46 4F)
        TRANS_REC: 0x53, // autosend timestamp (online control)
        TRANS_TIME: 0x54, // autosend timestamp (lightbeam trigger)
        GET_SI6: 0x61, // read out SI-card 6 data
        SI6_DET: 0x66, // SI-card 6 inserted
        SET_MS: 0x70, // \x4D="M"aster, \x53="S"lave
        GET_MS: 0x71,
        SET_SYS_VAL: 0x72,
        GET_SYS_VAL: 0x73,
        GET_BDATA: 0x74, // Note: response carries '\xC4'!
        ERASE_BDATA: 0x75,
        SET_TIME: 0x76,
        GET_TIME: 0x77,
        OFF: 0x78,
        GET_BDATA2: 0x7A, // Note: response carries '\xCA'!
        SET_BAUD: 0x7E, // 0=4800 baud, 1=38400 baud
    },
    basicCmdLookup: () => {
        if (si.proto._basicCmdLookup) {
            return si.proto._basicCmdLookup;
        }
        si.proto._basicCmdLookup = {};
        Object.keys(si.proto.basicCmd).map((k) => {
            si.proto._basicCmdLookup[si.proto.basicCmd[k]] = k;
        });
        return si.proto._basicCmdLookup;
    },

    // Extended protocol commands
    cmd: {
        GET_BACKUP: 0x81,
        SET_SYS_VAL: 0x82,
        GET_SYS_VAL: 0x83,
        SRR_WRITE: 0xA2, // ShortRangeRadio - SysData write
        SRR_READ: 0xA3, // ShortRangeRadio - SysData read
        SRR_QUERY: 0xA6, // ShortRangeRadio - network device query
        SRR_PING: 0xA7, // ShortRangeRadio - heartbeat from linked devices, every 50 seconds
        SRR_ADHOC: 0xA8, // ShortRangeRadio - ad-hoc message, f.ex. from SI-ActiveCard
        GET_SI5: 0xB1, // read out SI-card 5 data
        TRANS_REC: 0xD3, // autosend timestamp (online control)
        CLEAR_CARD: 0xE0, // found on SI-dev-forum: 02 E0 00 E0 00 03 (http://www.sportident.com/en/forum/8/56#59)
        GET_SI6: 0xE1, // read out SI-card 6 data block
        SET_SI6: 0xE2, // write SI-card 6 line (16 bytes)
        SET_SI6_SPECIAL: 0xE4, // write SI-card 6 special fields (e.g. start number)
        SI5_DET: 0xE5, // SI-card 5 inserted
        SI6_DET: 0xE6, // SI-card 6 inserted
        SI_REM: 0xE7, // SI-card removed
        SI8_DET: 0xE8, // SI-card 8/9/10/11/p/t inserted
        SET_SI8: 0xEA, // write SI-card 8/9/10/11/p/t data word
        GET_SI8: 0xEF, // read out SI-card 8/9/10/11/p/t data block
        SET_MS: 0xF0, // \x4D="M"aster, \x53="S"lave
        GET_MS: 0xF1,
        ERASE_BDATA: 0xF5,
        SET_TIME: 0xF6,
        GET_TIME: 0xF7,
        OFF: 0xF8,
        SIGNAL: 0xF9, // 02 F9 (number of signals) (CRC16) 03
        SET_BAUD: 0xFE, // \x00=4800 baud, \x01=38400 baud
    },
    cmdLookup: () => {
        if (si.proto._cmdLookup) {
            return si.proto._cmdLookup;
        }
        si.proto._cmdLookup = {};
        Object.keys(si.proto.cmd).map((k) => {
            si.proto._cmdLookup[si.proto.cmd[k]] = k;
        });
        return si.proto._cmdLookup;
    },

    // Protocol Parameters
    P_MS_DIRECT: 0x4D, // "M"aster
    P_MS_INDIRECT: 0x53, // "S"lave
    P_SI6_CB: 0x08,

    // offsets in system data
    // currently only O_MODE, O_STATION_CODE and O_PROTO are used
    sysDataOffset: {
        OLD_SERIAL: 0x00, // 2 bytes - only up to BSx6, numbers < 65.536
        OLD_CPU_ID: 0x02, // 2 bytes - only up to BSx6, numbers < 65.536
        SERIAL_NO: 0x00, // 4 bytes - only after BSx7, numbers > 70.000 (if byte 0x00 > 0, better use OLD offsets)
        FIRMWARE: 0x05, // 3 bytes
        BUILD_DATE: 0x08, // 3 bytes - YYMMDD
        MODEL_ID: 0x0B, // 2 bytes:
        //   8003: BSF3 (serial numbers > 1.000)
        //   8004: BSF4 (serial numbers > 10.000)
        //   8084: BSM4-RS232
        //   8086: BSM6-RS232 / BSM6-USB
        //   8146: BSF6 (serial numbers > 30.000)
        //   8115: BSF5 (serial numbers > 50.000)
        //   8117 / 8118: BSF7 / BSF8 (serial no. 70.000...70.521, 72.002...72.009)
        //   8187 / 8188: BS7-SI-Master / BS8-SI-Master
        //   8197: BSF7 (serial numbers > 71.000, apart from 72.002...72.009)
        //   8198: BSF8 (serial numbers > 80.000)
        //   9197 / 9198: BSM7-RS232, BSM7-USB / BSM8-USB, BSM8-SRR
        //   9199: unknown
        //   9597: BS7-S
        //   B197 / B198: BS7-P / BS8-P
        //   B897: BS7-GSM
        MEM_SIZE: 0x0D, // 1 byte - in KB
        REFRESH_RATE: 0x10, // 1 byte - in 3/sec
        POWER_MODE: 0x11, // 1 byte - 06 low power, 08 standard/sprint

        BAT_DATE: 0x15, // 3 bytes - YYMMDD
        BAT_CAP: 0x19, // 2 bytes - battery capacity in mAh (as multiples of 14.0625?!)
        BACKUP_PTR: 0x1C, // 4 bytes - at positions 1C,1D,21,22
        SI6_CB: 0x33, // 1 byte - bitfield defining which SI Card 6 blocks to read: \x00=\xC1=read block0,6,7; \x08=\xFF=read all 8 blocks
        BAT_STATE: 0x34, // 4 bytes - for battery state: 2000mAh: 000000=0%, 6E0000=100%, 1000mAh:000000=0%, 370000=100%
        MEM_OVERFLOW: 0x3D, // 1 byte - memory overflow if != 0x00

        INTERVAL: 0x48, // 2 bytes - in 32*ms
        WTF: 0x4A, // 2 bytes - in 32*ms

        PROGRAM: 0x70, // 1 byte - station program: xx0xxxxxb competition, xx1xxxxxb training
        MODE: 0x71, // 1 byte - see SI station modes below
        STATION_CODE: 0x72, // 1 byte
        PROTO: 0x74, // 1 byte - protocol configuration, bit mask value:
        //   xxxxxxx1b extended protocol
        //   xxxxxx1xb auto send out
        //   xxxxx1xxb handshake (only valid for card readout)
        //   xxxx1xxxb sprint 4ms (only for Start&Finish modes)
        //   xxx1xxxxb access with password only
        //   xx1xxxxxb stop, if backup is full (only for Readout mode)
        //   1xxxxxxxb read out SI-card after punch (only for punch modes;
        //             depends on bit 2: auto send out or handshake)
        LAST_WRITE_DATE: 0x75, // 3 bytes - YYMMDD
        LAST_WRITE_TIME: 0x78, // 3 bytes - 1 byte day (see below), 2 bytes seconds after midnight/midday
        SLEEP_TIME: 0x7B, // 3 bytes - 1 byte day (see below), 2 bytes seconds after midnight/midday
        //   xxxxxxx0b - seconds relative to midnight/midday: 0 = am, 1 = pm
        //   xxxx000xb - day of week: 000 = Sunday, 110 = Saturday
        //   xx00xxxxb - week counter 0..3, relative to programming date
        WORKING_TIME: 0x7E, // 2 bytes - big endian number = minutes
    },

    // SI station modes
    M_CONTROL: 0x02,
    M_START: 0x03,
    M_FINISH: 0x04,
    M_READ_SICARDS: 0x05,
    M_CLEAR: 0x07,
    M_CHECK: 0x0A,
    M_PRINTOUT: 0x0B,

    // Weekday encoding (only for reference, currently unused)
    D_SUNDAY: 0b000,
    D_MONDAY: 0b001,
    D_TUESDAY: 0b010,
    D_WEDNESDAY: 0b011,
    D_THURSDAY: 0b100,
    D_FRIDAY: 0b101,
    D_SATURDAY: 0b110,

    // Backup memory record length
    REC_LEN: 8, // Only in extended protocol, otherwise 6!

    // punch trigger in control mode data structure
    T_OFFSET: 8,
    T_CN: 0,
    T_TIME: 5,

    // backup memory in control mode
    BC_CN: 3,
    BC_TIME: 8,
};


// ###### tools ######

si.tools = {
    arr2big: (arr) => {
        var outnum = 0;
        for (var i = 0; i < arr.length; i++) { outnum += arr[i] * Math.pow(0x100, arr.length - i - 1); }
        return outnum;
    },
    arr2time: (arr) => {
        if (arr.length == 2) {
            if (arr[0] == 0xEE && arr[1] == 0xEE) {
                return null;
            }
            return si.tools.arr2big(arr);
        }
        console.warn(`arr2time: length must be 2, but is ${arr.length}`);
        return -2;

    },
    arr2date: (arr) => {
        if (arr.length == 7 || arr.length == 6) {
            var secs = si.tools.arr2big(arr.slice(4, 6));
            return new Date(arr[0] + 2000, arr[1] - 1, arr[2], (arr[3] & 0x01) * 12 + Math.floor(secs / 3600), Math.floor((secs % 3600) / 60), secs % 60, (arr.length == 7 ? arr[6] * 1000 / 256 : 0));
        } else if (arr.length == 3) {
            return new Date(2000 + arr[0], arr[1] - 1, arr[2]);
        }
        console.warn(`arr2date: length must be 3, 6 or 7, but is ${arr.length}`);
        return new Date(1970, 1, 1);

    },
    arr2cardNumber: (arr) => {
        if (arr.length == 4 || arr.length == 3) {
            var cardnum = (arr[1] << 8);
            cardnum |= arr[0];
            var fourthSet = (arr.length == 4 && arr[3] != 0x00);
            if (!fourthSet && 1 < arr[2] && arr[2] < 5) {
                cardnum += (arr[2] * 100000);
            } else if (fourthSet || 4 < arr[2]) {
                cardnum += (arr[2] << 16);
            }
            if (3 < arr.length) { cardnum |= (arr[3] << 24); }
            return cardnum;
        }
        console.warn(`arr2cardNumber: length must be 3 or 4, but is ${arr.length}`);
        return -1;

    },
    prettyHex: (str) => {
        var outstr = '';
        if (typeof str === 'string') {
            let i;
            for (i = 0; i < str.length; i++) {
                outstr += `${(`00${str.charCodeAt(i).toString(16)}`).slice(-2)} `;
            }
        } else {
            let i;
            for (i = 0; i < str.length; i++) {
                outstr += `${(`00${str[i].toString(16)}`).slice(-2)} `;
            }
        }
        return outstr;
    },
    CRC16: (str) => {
        var CRC_POLYNOM = 0x8005;
        var CRC_BITF = 0x8000;
        if (str.length < 3) {
            return [(1 <= str.length ? str[0] : 0x00), (2 <= str.length ? str[1] : 0x00)];
        }
        const s = str.length % 2 == 0 ? str.concat([0x00, 0x00]) : str.concat([0x00]);
        var crc = s[0] * 0x100 + s[1];
        for (var i = 2; i < s.length; i += 2) {
            var c = s.slice(i, i + 2);
            var val = c[0] * 0x100 + c[1];
            for (var j = 0; j < 16; j++) {
                if ((crc & CRC_BITF) != 0) {
                    crc = (crc << 1);
                    if ((val & CRC_BITF) != 0) { crc += 1; }
                    crc = (crc ^ CRC_POLYNOM);
                } else {
                    crc = (crc << 1);
                    if ((val & CRC_BITF) != 0) { crc += 1; }
                }
                val = (val << 1);
            }
            crc = (crc & 0xFFFF);
        }
        return [(crc >> 8), (crc & 0xFF)];
    },
};


// ###### Station ######

class SiStation {
    constructor(mainStation) {
        this.mainStation = mainStation;
        this._info = {};
        this._infoTime = 0;
        this._infoSubscribers = [];
    }

    static modeLookup() {
        if (SiStation._modeLookup) { return SiStation._modeLookup; }
        SiStation._modeLookup = {};
        Object.keys(SiStation.Mode).map((k) => {
            SiStation._modeLookup[SiStation.Mode[k].val] = k;
        });
        return SiStation._modeLookup;
    }

    static typeLookup() {
        if (SiStation._typeLookup) { return SiStation._typeLookup; }
        SiStation._typeLookup = {};
        Object.keys(SiStation.Type).map((k) => {
            SiStation._typeLookup[SiStation.Type[k].val] = k;
        });
        return SiStation._typeLookup;
    }

    static modelLookup() {
        if (SiStation._modelLookup) { return SiStation._modelLookup; }
        SiStation._modelLookup = {};
        Object.keys(SiStation.Model).map((k) => {
            SiStation.Model[k].vals.map((val) => {
                SiStation._modelLookup[val] = k;
            });
        });
        return SiStation._modelLookup;
    }

    readInfo(force) {
        var now = new Date().getTime();
        if (!force && now < this._infoTime + 60000) {
            return si.timeoutResolvePromise(this._info);
        }
        if (!force && 0 < this._infoSubscribers.length) {
            return new Promise((resolve, reject) => {
                this._infoSubscribers.push({resolve: resolve, reject: reject});
            });
        }
        return this.mainStation._sendCommand(si.proto.cmd.GET_SYS_VAL, [0x00, 0x80], 1)
            .then((d) => {
                const data = d[0];
                data.splice(0, 3);
                this._infoTime = new Date().getTime();
                this._info = {};
                this._info._raw = data;
                this._info.serialNumber = si.tools.arr2big(data.slice(0x00, 0x04));
                this._info.firmwareVersion = si.tools.arr2big(data.slice(0x05, 0x08));
                this._info.buildDate = si.tools.arr2date(data.slice(0x08, 0x0B));
                this._info.deviceModel = SiStation.modelLookup()[si.tools.arr2big(data.slice(0x0B, 0x0D))];
                this._info.memorySize = si.tools.arr2big(data.slice(0x0D, 0x0E));
                this._info.refreshRate = data[0x10]; // in 3/sec
                this._info.powerMode = data[0x11]; // 06 low power, 08 standard/sprint
                this._info.batteryDate = si.tools.arr2date(data.slice(0x15, 0x18));
                this._info.batteryCapacity = si.tools.arr2big(data.slice(0x19, 0x1B));
                this._info.backupPointer = si.tools.arr2big(data.slice(0x1C, 0x1E).concat(data.slice(0x21, 0x23)));
                this._info.siCard6Mode = si.tools.arr2big(data.slice(0x33, 0x34));
                this._info.memoryOverflow = si.tools.arr2big(data.slice(0x3D, 0x3E));
                this._info.interval = si.tools.arr2big(data.slice(0x48, 0x4A));
                this._info.wtf = si.tools.arr2big(data.slice(0x4A, 0x4C));
                this._info.program = data[0x70];
                this._info.mode = SiStation.modeLookup()[data[0x71]];
                this._info.code = data[0x72] + ((data[0x73] & 0xC0) << 2);
                this._info.beeps = ((data[0x73] >> 2) & 0x01);
                this._info.flashes = (data[0x73] & 0x01);
                this._info.extendedProtocol = (data[0x74] & 0x01);
                this._info.autoSend = ((data[0x74] >> 1) & 0x01);
                this._info.handshake = ((data[0x74] >> 2) & 0x01);
                this._info.sprint4ms = ((data[0x74] >> 3) & 0x01);
                this._info.passwordOnly = ((data[0x74] >> 4) & 0x01);
                this._info.stopOnFullBackup = ((data[0x74] >> 5) & 0x01);
                this._info.autoReadout = ((data[0x74] >> 7) & 0x01);
                this._info.lastWriteDate = si.tools.arr2date(data.slice(0x75, 0x7B));
                // this._info.autoOffTimeout = si.tools.arr2date([0, 1, 1].concat(data.slice(0x7B, 0x7E)));
                this._info.autoOffTimeout = si.tools.arr2big(data.slice(0x7E, 0x80));
                this._infoSubscribers.map((infoSubscriber) => setTimeout(() => infoSubscriber.resolve(this._info), 1));
                this._infoSubscribers = [];
                console.log('INFO READ', this._info);
                return this._info;
            })
            .catch((_err) => {
                this._infoSubscribers.map((infoSubscriber) => setTimeout(() => infoSubscriber.reject(), 1));
                this._infoSubscribers = [];
                throw new Error('READ INFO');
            });
    }

    time(newTime) {
        if (newTime === undefined) {
            return this.mainStation._sendCommand(si.proto.cmd.GET_TIME, [], 1)
                .then((d) => si.tools.arr2date(d[0].slice(2)));
        }
        // TODO: compensate for waiting time
        var secs = (newTime.getHours() % 12) * 3600 + newTime.getMinutes() * 60 + newTime.getSeconds();
        var params = [
            newTime.getFullYear() % 100,
            newTime.getMonth() + 1,
            newTime.getDate(),
            (newTime.getDay() << 1) + Math.floor(newTime.getHours() / 12),
            secs >> 8,
            secs & 0xFF,
            Math.floor(newTime.getMilliseconds() * 256 / 1000),
        ];
        return this.mainStation._sendCommand(si.proto.cmd.SET_TIME, params, 1)
            .then((d) => si.tools.arr2date(d[0].slice(2)));

    }

    signal(countArg) {
        const count = !countArg || countArg < 1 ? 1 : countArg;
        return this.mainStation._sendCommand(si.proto.cmd.SIGNAL, [count], 1)
            .then((data) => {
                if (data[0][2] !== count) {
                    throw new Error('NUM BEEPS');
                }
            });
    }

    powerOff() { // Does not power off BSM8 (USB powered), though
        return this.mainStation._sendCommand(si.proto.cmd.OFF, [], 0);
    }

    info(property, paramsFunc, newValue) {
        if (newValue === undefined) {
            return this.readInfo(false)
                .then((info) => info[property]);
        }
        let params = undefined;
        return this.readInfo(false)
            .then((info) => {
                params = paramsFunc(info);
                if (!params) {
                    throw new Error('INVALID_PARAM');
                }
                return this.mainStation._sendCommand(si.proto.cmd.SET_SYS_VAL, params, 1);
            })
            .then((d) => {
                const data = d[0];
                data.splice(0, 2);
                if (data[0] != params[0]) {
                    throw new Error('SET_CODE_RESP_ERR');
                }
                return this.readInfo(true);
            })
            .then((info) => info[property]);
    }

    // TODO: program (0x70)

    code(newCode) {
        return this.info('code', (info) => [0x72, newCode & 0xFF, ((newCode & 0x0300) >> 2) + (info._raw[0x73] & 0x3F)], newCode);
    }

    mode(newMode) {
        return this.info('mode', (_info) => {
            var modeLookup = SiStation.modeLookup();
            if (modeLookup[newMode] == undefined) {
                return false;
            }
            return [0x71, newMode];
        }, newMode);
    }

    beeps(newBeeps) {
        return this.info('beeps', (info) => [0x73, (newBeeps ? 0x04 : 0x00) + (info._raw[0x73] & 0xFB)], newBeeps);
    }

    flashes(newFlashes) {
        return this.info('flashes', (info) => [0x73, (newFlashes ? 0x01 : 0x00) + (info._raw[0x73] & 0xFE)], newFlashes);
    }

    autoSend(newAutoSend) {
        return this.info('autoSend', (info) => [0x74, (newAutoSend ? 0x02 : 0x00) + (info._raw[0x74] & 0xFD)], newAutoSend);
    }

    extendedProtocol(newExtendedProtocol) {
        return this.info('extendedProtocol', (info) => [0x74, (newExtendedProtocol ? 0x01 : 0x00) + (info._raw[0x74] & 0xFE)], newExtendedProtocol);
    }

    serialNumber() {
        return this.info('serialNumber', () => false, undefined);
    }

    firmwareVersion() {
        return this.info('firmwareVersion', () => false, undefined);
    }

    buildDate() {
        return this.info('buildDate', () => false, undefined);
    }

    deviceModel() {
        return this.info('deviceModel', () => false, undefined);
    }

    memorySize() {
        return this.info('memorySize', () => false, undefined);
    }

    batteryDate() {
        return this.info('batteryDate', () => false, undefined);
    }

    batteryCapacity() {
        return this.info('batteryCapacity', () => false, undefined);
    }

    backupPointer() {
        return this.info('backupPointer', () => false, undefined);
    }

    siCard6Mode() {
        return this.info('siCard6Mode', () => false, undefined);
    }

    memoryOverflow() {
        return this.info('memoryOverflow', () => false, undefined);
    }

    lastWriteDate() {
        return this.info('lastWriteDate', () => false, undefined);
    }

    autoOffTimeout() {
        return this.info('autoOffTimeout', () => false, undefined);
    }
}
SiStation.Mode = {
    SIACSpecialFunction1: {val: 0x01},
    Control: {val: 0x02},
    Start: {val: 0x03},
    Finish: {val: 0x04},
    Readout: {val: 0x05},
    Clear: {val: 0x07},
    Check: {val: 0x0A},
    Print: {val: 0x0B},
    StartWithTimeTrigger: {val: 0x0C},
    FinishWithTimeTrigger: {val: 0x0D},
    BCControl: {val: 0x12},
    BCStart: {val: 0x13},
    BCFinish: {val: 0x14},
    BCSlave: {val: 0x1F},
};

SiStation.Type = { // TODO: meaningful val-s
    Main: {val: 0x00},
    Sprint: {val: 0x01},
    Print: {val: 0x02},
    Field: {val: 0x03},
    Master: {val: 0x04},
};

SiStation.Model = {
    BSF3: {vals: [0x8003], description: 'BSF3', type: SiStation.Type.Field, series: 3},
    BSF4: {vals: [0x8004], description: 'BSF4', type: SiStation.Type.Field, series: 4},
    BSF5: {vals: [0x8115], description: 'BSF5', type: SiStation.Type.Field, series: 5},
    BSF6: {vals: [0x8146], description: 'BSF6', type: SiStation.Type.Field, series: 6},
    BSF7: {vals: [0x8117, 0x8197], description: 'BSF7', type: SiStation.Type.Field, series: 7},
    BSF8: {vals: [0x8118, 0x8198], description: 'BSF8', type: SiStation.Type.Field, series: 8},
    BS7Master: {vals: [0x8187], description: 'BS7-Master', type: SiStation.Type.Master, series: 7},
    BS8Master: {vals: [0x8188], description: 'BS8-Master', type: SiStation.Type.Master, series: 8},
    BSM4: {vals: [0x8084], description: 'BSM4', type: SiStation.Type.Main, series: 4},
    BSM6: {vals: [0x8086], description: 'BSM6', type: SiStation.Type.Main, series: 6},
    BSM7: {vals: [0x9197], description: 'BSM7', type: SiStation.Type.Main, series: 7},
    BSM8: {vals: [0x9198], description: 'BSM8', type: SiStation.Type.Main, series: 8},
    BS7S: {vals: [0x9597], description: 'BS7-S', type: SiStation.Type.Sprint, series: 7},
    BS7P: {vals: [0xB197], description: 'BS7-P', type: SiStation.Type.Print, series: 7},
    BS7GSM: {vals: [0xB897], description: 'BS7-GSM', type: SiStation.Type.Field, series: 7},
    BS8P: {vals: [0xB198], description: 'BS8-P', type: SiStation.Type.Print, series: 8},
};

si.Station = SiStation;


// ###### MainStation ######

class SiMainStation extends SiStation {
    constructor(device, state) {
        super(undefined);
        this.mainStation = this;
        this.device = device;
        this.card = false;
        this.onRemoved = false;
        this.onStateChanged = false;
        this.onCardInserted = false;
        this.onCard = false;
        this.onCardRemoved = false;
        this._sendQueue = [];
        this._respBuffer = [];
        this._deviceOpenTimer = false;
        this._deviceOpenNumErrors = 0;
        this.state = state;
        if (!this.state) { this.state = SiMainStation.State.Uninitialized; }
        if (!SiMainStation.allByDevice[device.ident]) {
            SiMainStation.allByDevice[device.ident] = this;
            try {
                SiMainStation.onAdded(this);
            } catch (err) {
                // ignore
            }
        }
        if (this.state == SiMainStation.State.Uninitialized) { this._deviceOpen(); }
    }

    activate() {
        if (this.state != SiMainStation.State.Proposed) { return false; }
        this.state = SiMainStation.State.Uninitialized;
        try {
            this.onStateChanged(this.state);
        } catch (err) {
            // ignore
        }
        this._deviceOpen();
        return true;
    }

    _changeState(newState) {
        this.state = newState;
        if (this.onStateChanged) {
            this.onStateChanged(this.state);
        }
    }

    _deviceOpen() {
        this.device.driver.open(this)
            .then(() => {
                this._deviceOpenNumErrors = 0;
                this.state = SiMainStation.State.Ready;
                this._sendCommand(si.proto.cmd.GET_MS, [0x00], 1, 5)
                    .then(() => {
                        this._changeState(SiMainStation.State.Ready);
                    })
                    .catch((err) => {
                        this._changeState(SiMainStation.State.Uninitialized);
                        console.error('Could not communicate after having opened SiMainStation: ', err);
                        this._retryDeviceOpen();
                    });
            })
            .catch((err) => {
                console.error('Could not open SiMainStation: ', err);
                this._retryDeviceOpen();
            });
    }

    _retryDeviceOpen() {
        var scheduleReopen = () => {
            if (!this._deviceOpenTimer) {
                var timeout = 100;
                for (var i = 0; i < this._deviceOpenNumErrors && i < 10; i++) { timeout = timeout * 2; }
                this._deviceOpenTimer = window.setTimeout(() => {
                    this._deviceOpenTimer = false;
                    this._deviceOpen();
                }, timeout);
                this._deviceOpenNumErrors++;
            }
        };
        this.device.driver.close(this)
            .then(() => {
                scheduleReopen();
            })
            .catch((err) => {
                console.error('Could not close device: ', err);
                scheduleReopen();
            });
    }

    _logReceive(bufView) {
        console.debug(`<= ${si.tools.prettyHex(bufView)}     (${this.device.driver.name})`);
    }

    _processRespBuffer() {
        while (0 < this._respBuffer.length) {
            if (this._respBuffer[0] == si.proto.ACK) {
                this._respBuffer.splice(0, 1);
            } else if (this._respBuffer[0] == si.proto.NAK) {
                this._respBuffer.splice(0, 1);
                if (0 < this._sendQueue.length && this._sendQueue[0].state != -1) {
                    if (this._sendQueue[0].timeoutTimer) { window.clearTimeout(this._sendQueue[0].timeoutTimer); }
                    this._sendQueue[0].reject('NAK');
                    this._sendQueue.shift();
                }
            } else if (this._respBuffer[0] == si.proto.WAKEUP) {
                this._respBuffer.splice(0, 1);
            } else if (this._respBuffer[0] == si.proto.STX) {
                if (this._respBuffer.length < 6) { break; }
                var command = this._respBuffer[1];
                var len = this._respBuffer[2];
                if (this._respBuffer.length < 6 + len) { break; }
                if (this._respBuffer[5 + len] != si.proto.ETX) {
                    console.warn('Invalid end byte. Flushing buffer.');
                    this._respBuffer = [];
                    break;
                }
                var parameters = this._respBuffer.slice(3, 3 + len);
                var crcContent = si.tools.CRC16(this._respBuffer.slice(1, 3 + len));
                var crc = this._respBuffer.slice(3 + len, 5 + len);
                this._respBuffer.splice(0, 6 + len);
                if (crc[0] == crcContent[0] && crc[1] == crcContent[1]) {
                    // console.debug("Valid Command received.  CMD:0x"+si.tools.prettyHex([command])+" LEN:"+len+"  PARAMS:"+si.tools.prettyHex(parameters)+" CRC:"+si.tools.prettyHex(crc)+" Content-CRC:"+si.tools.prettyHex(crcContent));
                    let cn, typeFromCN;
                    if (command == si.proto.cmd.SI5_DET) {
                        cn = si.tools.arr2big([parameters[3], parameters[4], parameters[5]]);
                        if (499999 < cn) { console.warn(`card5 Error: SI Card Number inconsistency: SI5 detected, but number is ${cn} (not in 0 - 500'000)`); }
                        if (parameters[3] < 2) { cn = si.tools.arr2big([parameters[4], parameters[5]]); } else { cn = parameters[3] * 100000 + si.tools.arr2big([parameters[4], parameters[5]]); }
                        this.card = new SiCard(this, cn);
                        console.log('SI5 DET', this.card, parameters);
                        window.setTimeout(() => {
                            if (this.onCardInserted) { this.onCardInserted(this.card); }
                        }, 1);
                        this.card.read()
                            .then((card) => {
                                if (this.onCard) {
                                    this.onCard(card);
                                }
                            });
                    } else if (command == si.proto.cmd.SI6_DET) {
                        cn = si.tools.arr2big([parameters[3], parameters[4], parameters[5]]);
                        typeFromCN = SiCard.typeByCardNumber(cn);
                        if (typeFromCN != 'SICard6') { console.warn(`SICard6 Error: SI Card Number inconsistency: Function SI6 called, but number is ${cn} (=> ${typeFromCN})`); }
                        this.card = new SiCard(this, cn);
                        console.log('SI6 DET', parameters);
                        window.setTimeout(() => {
                            if (this.onCardInserted) { this.onCardInserted(this.card); }
                        }, 1);
                        this.card.read()
                            .then((card) => {
                                if (this.onCard) {
                                    this.onCard(card);
                                }
                            });
                    } else if (command == si.proto.cmd.SI8_DET) {
                        cn = si.tools.arr2big([parameters[3], parameters[4], parameters[5]]);
                        typeFromCN = SiCard.typeByCardNumber(cn);
                        if (!{'SICard8': 1, 'SICard9': 1, 'SICard10': 1, 'SICard11': 1}[typeFromCN]) { console.warn(`SICard8 Error: SI Card Number inconsistency: Function SI8 called, but number is ${cn} (=> ${typeFromCN})`); }
                        this.card = new SiCard(this, cn);
                        console.log('SI8 DET', parameters);
                        window.setTimeout(() => {
                            if (this.onCardInserted) { this.onCardInserted(this.card); }
                        }, 1);
                        this.card.read()
                            .then((card) => {
                                if (this.onCard) {
                                    this.onCard(card);
                                }
                            });
                    } else if (command == si.proto.cmd.SI_REM) {
                        console.log('SI REM', parameters);
                        if (0 < this._sendQueue.length && this._sendQueue[0].state != -1 && 0xB0 <= this._sendQueue[0].command && this._sendQueue[0].command <= 0xEF) { // Was expecting response from card => "early Timeout"
                            if (this._sendQueue[0].timeoutTimer) { window.clearTimeout(this._sendQueue[0].timeoutTimer); }
                            this._sendQueue[0].reject('TIMEOUT');
                            console.debug(`Early Timeout: cmd ${si.tools.prettyHex([this._sendQueue[0].command])} (expected ${this._sendQueue[0].numResp} responses)`, this._sendQueue[0].bufResp);
                            this._sendQueue.shift();
                        }
                        // this._sendCommand(si.proto.ACK, [], 0);
                        window.setTimeout(() => {
                            if (this.onCardRemoved) { this.onCardRemoved(this.card); }
                        }, 1);
                        this.card = false;
                    } else if (command == si.proto.cmd.TRANS_REC) {
                        cn = si.tools.arr2big([parameters[3], parameters[4], parameters[5]]);
                        if (cn < 500000) {
                            if (parameters[3] < 2) { cn = si.tools.arr2big([parameters[4], parameters[5]]); } else { cn = parameters[3] * 100000 + si.tools.arr2big([parameters[4], parameters[5]]); }
                        }
                        this.card = new SiCard(this, cn);
                        console.log('TRANS_REC', this.card, parameters);
                        // this._sendCommand(si.proto.ACK, [], 0);
                        window.setTimeout(() => {
                            if (this.onCardInserted) { this.onCardInserted(this.card); }
                        }, 1);
                        window.setTimeout(() => {
                            if (this.onCardRemoved) { this.onCardRemoved(this.card); }
                        }, 1);
                        this.card = false;
                    } else if (0 < this._sendQueue.length && this._sendQueue[0].state != -1) { // We are expecting a certain response
                        if (this._sendQueue[0].command == command) { // It was, what we were expecting
                            this._sendQueue[0].bufResp.push(parameters);
                            if (this._sendQueue[0].bufResp.length == this._sendQueue[0].numResp) { // TODO: some kind of onProgress, or just call onSuccess with incomplete buf?
                                if (this._sendQueue[0].timeoutTimer) {
                                    window.clearTimeout(this._sendQueue[0].timeoutTimer);
                                }
                                this._sendQueue[0].resolve(this._sendQueue[0].bufResp);
                                this._sendQueue.shift();
                            }
                            // Continue sending
                            window.setTimeout(() => this._processSendQueue(), 1);
                        } else {
                            console.warn(`Strange Response: expected ${si.tools.prettyHex([this._sendQueue[0].command])}, but got ${si.tools.prettyHex([command])}...`);
                        }
                    } else {
                        console.warn(`Strange Response: ${si.tools.prettyHex([command])} (not expecting anything)...`);
                    }
                } else {
                    console.debug(`Invalid Command received.  CMD:0x${si.tools.prettyHex([command])} LEN:${len}  PARAMS:${si.tools.prettyHex(parameters)} CRC:${si.tools.prettyHex(crc)} Content-CRC:${si.tools.prettyHex(crcContent)}`);
                }
            } else {
                console.warn('Invalid start byte', this._respBuffer[0]);
                this._respBuffer.splice(0, 1);
            }
        }
    }

    _processSendQueue() {
        if (0 < this._sendQueue.length && this._sendQueue[0].state == -1) {
            var request = this._sendQueue[0];
            // Build command
            var commandString = [request.command, request.parameters.length].concat(request.parameters);
            var crc = si.tools.CRC16(commandString);
            var cmd = String.fromCharCode(si.proto.STX);
            let i;
            for (i = 0; i < commandString.length; i++) {
                cmd += String.fromCharCode(commandString[i]);
            }
            for (i = 0; i < crc.length; i++) {
                cmd += String.fromCharCode(crc[i]);
            }
            cmd += String.fromCharCode(si.proto.ETX);

            // Send command
            var bstr = String.fromCharCode(si.proto.WAKEUP) + cmd;
            var bytes = new Uint8Array(bstr.length);
            for (i = 0; i < bstr.length; i++) {
                bytes[i] = bstr.charCodeAt(i);
            }
            this.device.driver.send(this, bytes.buffer)
                .then(() => {
                    console.debug(`=> ${si.tools.prettyHex(bstr)}     (${this.device.driver.name})`);
                })
                .catch((err) => {
                    console.error(err);
                });
            request.state = 0;

            // Response handling setup
            if (request.numResp <= 0) {
                request.resolve([]);
                this._sendQueue.shift();
                window.setTimeout(() => this._processSendQueue(), 1);
            }
        }
    }

    _sendCommand(command, parameters, numRespArg, timeoutArg) {
        return new Promise((resolve, reject) => {
            // Default values
            const numResp = numRespArg ? numRespArg : 0;
            const timeout = timeoutArg ? timeoutArg : 10;

            // If not ready => fail
            if (this.state != SiMainStation.State.Ready) {
                window.setTimeout(() => reject(new Error('NOT READY')), 1);
                return;
            }

            const sendTask = {
                command: command,
                parameters: parameters,
                numResp: numResp,
                resolve: resolve,
                reject: reject,
                timeout: timeout,
                state: -1,
                timeoutTimer: undefined,
                bufResp: [],
            };

            sendTask.timeoutTimer = window.setTimeout(() => {
                if (0 < this._sendQueue.length && this._sendQueue[0] === sendTask) {
                    window.clearTimeout(sendTask.timeoutTimer);
                    sendTask.reject('TIMEOUT');
                    console.debug(`Timeout: cmd ${si.tools.prettyHex([sendTask.command])} (expected ${sendTask.numResp} responses)`, sendTask.bufResp);
                    this._sendQueue.shift();
                    window.setTimeout(() => {
                        this._processSendQueue();
                    }, 1);
                }
            }, timeout * 1000);

            // Add to Queue
            this._sendQueue.push(sendTask); // State: -1=notYetStarted, 0=sentButNoResp, X=XRespReceived
            this._processSendQueue();
        });
    }

    _remove() {
        if (0 < this._sendQueue.length && this._sendQueue[0].state != -1) { window.clearTimeout(this._sendQueue[0].timeoutTimer); }
        window.clearTimeout(this._deviceOpenTimer);
        delete SiMainStation.allByDevice[this.device.ident];
        try {
            SiMainStation.onRemoved(this);
        } catch (err) {
            // ignore
        }
        try {
            this.onRemoved();
        } catch (err) {
            // ignore
        }
    }
}

SiMainStation.State = { // TODO: maybe include instructions in description?
    Proposed: {val: 0, description: 'This SiMainStation is proposed to be selected by the user.'},
    Uninitialized: {val: 1, description: 'This SiMainStation is not yet initialized. Commands can neither be received nor sent yet.'},
    Ready: {val: 2, description: 'This SiMainStation is initialized and ready. Commands can be received and sent.'},
};

SiMainStation.allByDevice = {};
SiMainStation.all = () => {
    var arr = [];
    Object.keys(SiMainStation.allByDevice).map((deviceIdent) => {
        arr.push(SiMainStation.allByDevice[deviceIdent]);
    });
    return arr;
};
SiMainStation.drivers = {};
SiMainStation.startDeviceDetection = () => {
    var runDeviceDetection = () => {
        Object.keys(SiMainStation.drivers).map((k) => {
            try {
                var driver = SiMainStation.drivers[k];
                if (driver && driver.name && driver.detect && driver.send && driver.open && driver.close) {
                    driver.detect();
                } else {
                    console.warn('Not a driver:', k);
                }
            } catch (err) {
                console.warn('Error in device detection:', err);
            }
        });
        if (SiMainStation.detectionTimeout) { window.clearTimeout(SiMainStation.detectionTimeout); }
        SiMainStation.detectionTimeout = window.setTimeout(runDeviceDetection, 1000);
    };
    runDeviceDetection();
};
SiMainStation.newDevice = () => {
    Object.keys(SiMainStation.drivers).map((k) => {
        try {
            var driver = SiMainStation.drivers[k];
            if (driver && driver.name && driver.detect && driver.send && driver.open && driver.close) {
                driver.new();
            } else {
                console.warn('Not a driver:', k);
            }
        } catch (err) {
            console.warn('Error in device detection:', err);
        }
    });
};
SiMainStation.onAdded = (_ms) => undefined;
SiMainStation.onRemoved = (_ms) => undefined;

si.MainStation = SiMainStation;


// ###### Card ######

class SiCard {
    constructor(mainStation, cardNumber) {
        this.mainStation = mainStation;
        this.cardNumber = cardNumber;
        this.zeroTime = -1;
        this.clearTime = -1;
        this.checkTime = -1;
        this.startTime = -1;
        this.finishTime = -1;
        this.punches = [];
    }

    read() {
        var typeFromCN = SiCard.typeByCardNumber(this.cardNumber);
        return SiCard.Type[typeFromCN].read(this);
    }
}
SiCard.Type = {
    SICard5: {vals: [1000, 500000], description: 'SI Card 5', read: (card) =>
        card.mainStation._sendCommand(si.proto.cmd.GET_SI5, [], 1)
            .then((d) => {
                const data = d[0];
                data.splice(0, 2);
                var cn = si.tools.arr2big([data[6], data[4], data[5]]);
                if (499999 < cn) { console.warn(`SICard5 Error: SI Card Number inconsistency: SI5 detected, but number is ${cn} (not in 0 - 500'000)`); }
                if (data[6] < 2) {
                    cn = si.tools.arr2big([data[4], data[5]]);
                } else {
                    cn = data[6] * 100000 + si.tools.arr2big([data[4], data[5]]);
                }
                if (card.cardNumber != cn) { console.warn('SICard5 Error: SI Card Number inconsistency'); }

                card.startTime = si.tools.arr2time(data.slice(19, 21));
                card.finishTime = si.tools.arr2time(data.slice(21, 23));
                card.checkTime = si.tools.arr2time(data.slice(25, 27));
                // TODO: also read the 6(?) additional punch codes without times
                var len = Math.min(data[23] - 1, 30);
                card.punches = new Array(len);
                var ind = 32;
                for (var i = 0; i < len; i++) {
                    if ((ind % 16) == 0) { ind++; }
                    var time = si.tools.arr2time(data.slice(ind + 1, ind + 3));
                    if (0 <= time) { card.punches[i] = {code: data[ind + 0], time: time}; } else { console.warn('SICard5 Error: Undefined Time in punched range'); }
                    ind += 3;
                }
                card.mainStation._sendCommand(si.proto.ACK, [], 0);
                return card;
            }),
    },
    SICard6: {vals: [500000, 1000000, 2003000, 2004000], description: 'SI Card 6', read: (card) =>
        card.mainStation._sendCommand(si.proto.cmd.GET_SI6, [0x08], 3)
            .then((data) => {
                if (data[0][2] != 0) { console.warn(`SICard6 Error: First read block is ${data[0][2]} (expected 0)`); }
                if (data[1][2] != 6) { console.warn(`SICard6 Error: Second read block is ${data[1][2]} (expected 6)`); }
                if (data[2][2] != 7) { console.warn(`SICard6 Error: Third read block is ${data[2][2]} (expected 7)`); }
                data[0].splice(0, 3);
                data[1].splice(0, 3);
                data[2].splice(0, 3);
                var cn = si.tools.arr2big([data[0][11], data[0][12], data[0][13]]);
                if (card.cardNumber != cn) { console.warn('SICard6 Error: SI Card Number inconsistency'); }

                card.startTime = si.tools.arr2time(data[0].slice(26, 28));
                card.finishTime = si.tools.arr2time(data[0].slice(22, 24));
                card.checkTime = si.tools.arr2time(data[0].slice(30, 32));
                card.clearTime = si.tools.arr2time(data[0].slice(34, 36));
                var len = Math.min(data[0][18] - 1, 64);
                card.punches = new Array(len);
                var blk = 1;
                var ind = 0;
                for (var i = 0; i < len; i++) {
                    if (128 <= ind) {
                        blk++;
                        ind = 0;
                    }
                    var time = si.tools.arr2time(data[blk].slice(ind + 2, ind + 4));
                    if (0 <= time) { card.punches[i] = {code: data[blk][ind + 1], time: time}; } else { console.warn('SICard6 Error: Undefined Time in punched range'); }
                    ind += 4;
                }
                card.mainStation._sendCommand(si.proto.ACK, [], 0);
                return card;
            }),
    },
    SICard8: {vals: [2000000, 2003000, 2004000, 3000000], description: 'SI Card 8', read: (card) => {
        let len = undefined;
        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x00], 1)
            .then((data0) => {
                console.assert(data0[0][2] === 0, 'Inconsistent');
                const page0 = data0[0].slice(3);
                var cn = si.tools.arr2big([page0[25], page0[26], page0[27]]);
                if (card.cardNumber != cn) {
                    console.warn('SICard8 Error: SI Card Number inconsistency');
                }

                card.startTime = si.tools.arr2time(page0.slice(14, 16));
                card.finishTime = si.tools.arr2time(page0.slice(18, 20));
                card.checkTime = si.tools.arr2time(page0.slice(10, 12));
                len = Math.min(page0[22] - 1, 128);
                card.punches = new Array(len);

                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x01], 1);
            })
            .then((data1) => {
                console.assert(data1[0][2] === 1, 'Inconsistent');
                const page1 = data1[0].slice(3);
                for (let i = 0; i < 30; i++) {
                    if (i >= len) {
                        break;
                    }
                    var time = si.tools.arr2time(page1.slice(i * 4 + 10, i * 4 + 12));
                    if (0 <= time) {
                        card.punches[i] = {
                            code: page1[i * 4 + 9],
                            time: time,
                        };
                    } else {
                        console.warn('SICard8 Error: Undefined Time in punched range');
                    }
                }
                return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
            });
    }},
    SICard9: {vals: [1000000, 2000000], description: 'SI Card 9', read: (card) => {
        let len = undefined;
        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x00], 1)
            .then((data0) => {
                console.assert(data0[0][2] === 0, 'Inconsistent');
                const page0 = data0[0].slice(3);
                var cn = si.tools.arr2big([page0[25], page0[26], page0[27]]);
                if (card.cardNumber != cn) {
                    console.warn('SICard9 Error: SI Card Number inconsistency');
                }

                card.startTime = si.tools.arr2time(page0.slice(14, 16));
                card.finishTime = si.tools.arr2time(page0.slice(18, 20));
                card.checkTime = si.tools.arr2time(page0.slice(10, 12));
                len = Math.min(page0[22] - 1, 128);
                card.punches = new Array(len);

                let isLastBlock = false;
                for (let i = 0; i < 18; i++) {
                    if (i >= len) {
                        isLastBlock = true;
                        break;
                    }
                    const time = si.tools.arr2time(page0.slice(i * 4 + 58, i * 4 + 60));
                    if (0 <= time) {
                        card.punches[i] = {
                            code: page0[i * 4 + 57],
                            time: time,
                        };
                    } else {
                        console.warn('SICard9 Error: Undefined Time in punched range');
                    }
                }
                if (isLastBlock) {
                    return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                }

                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x01], 1).then((data1) => {
                    console.assert(data1[0][2] === 1, 'Inconsistent');
                    const page1 = data1[0].slice(3);
                    for (let i = 18; i < 50; i++) {
                        if (i >= len) {
                            break;
                        }
                        const time = si.tools.arr2time(page1.slice(i * 4 - 70, i * 4 - 68));
                        if (0 <= time) {
                            card.punches[i] = {
                                code: page1[i * 4 - 71],
                                time: time,
                            };
                        } else {
                            console.warn('SICard9 Error: Undefined Time in punched range');
                        }
                    }
                    return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                });
            });
    }},
    SICard10: {vals: [7000000, 8000000], description: 'SI Card 10', read: (card) => {
        const readSiCard10TimeBlock = (blockData, punchData, blockIndex, punchCount) => {
            let isLastBlock = false;
            const punchesPerBlock = 32;
            for (let i = 0; i < punchesPerBlock; i++) {
                if (blockIndex * punchesPerBlock + i >= punchCount) {
                    isLastBlock = true;
                    break;
                }
                var time = si.tools.arr2time(blockData.slice(i * 4 + 2, i * 4 + 4));
                if (0 <= time) {
                    punchData[blockIndex * punchesPerBlock + i] = {
                        code: blockData[i * 4 + 1],
                        time: time,
                    };
                } else {
                    console.warn('SICard10 Error: Undefined Time in punched range');
                }
            }
            return isLastBlock;
        };
        let len = undefined;
        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x00], 1)
            .then((data0) => {
                console.assert(data0[0][2] === 0, 'Inconsistent');
                const page0 = data0[0].slice(3);
                var cn = si.tools.arr2big([page0[25], page0[26], page0[27]]);
                if (card.cardNumber != cn) {
                    console.warn('SICard10 Error: SI Card Number inconsistency');
                }

                card.startTime = si.tools.arr2time(page0.slice(14, 16));
                card.finishTime = si.tools.arr2time(page0.slice(18, 20));
                card.checkTime = si.tools.arr2time(page0.slice(10, 12));
                len = Math.min(page0[22] - 1, 128);
                card.punches = new Array(len);

                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x04], 1);
            })
            .then((data4) => {
                console.assert(data4[0][2] === 4, 'Inconsistent');
                const page4 = data4[0].slice(3);
                const is4LastBlock = readSiCard10TimeBlock(page4, card.punches, 0, len);
                if (is4LastBlock) {
                    return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                }
                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x05], 1).then((data5) => {
                    console.assert(data5[0][2] === 5, 'Inconsistent');
                    const page5 = data5[0].slice(3);
                    const is5LastBlock = readSiCard10TimeBlock(page5, card.punches, 1, len);
                    if (is5LastBlock) {
                        return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                    }
                    return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x06], 1).then((data6) => {
                        console.assert(data6[0][2] === 6, 'Inconsistent');
                        const page6 = data6[0].slice(3);
                        const is6LastBlock = readSiCard10TimeBlock(page6, card.punches, 2, len);
                        if (is6LastBlock) {
                            return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                        }
                        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x07], 1).then((data7) => {
                            console.assert(data7[0][2] === 7, 'Inconsistent');
                            const page7 = data7[0].slice(3);
                            readSiCard10TimeBlock(page7, card.punches, 3, len);
                            return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                        });
                    });
                });
            });
    }},
    SICard11: {vals: [9000000, 10000000], description: 'SI Card 11', read: (card) => {
        const readSiCard11TimeBlock = (blockData, punchData, blockIndex, punchCount) => {
            let isLastBlock = false;
            const punchesPerBlock = 32;
            for (let i = 0; i < punchesPerBlock; i++) {
                if (blockIndex * punchesPerBlock + i >= punchCount) {
                    isLastBlock = true;
                    break;
                }
                var time = si.tools.arr2time(blockData.slice(i * 4 + 2, i * 4 + 4));
                if (0 <= time) {
                    punchData[blockIndex * punchesPerBlock + i] = {
                        code: blockData[i * 4 + 1],
                        time: time,
                    };
                } else {
                    console.warn('SICard11 Error: Undefined Time in punched range');
                }
            }
            return isLastBlock;
        };
        let len = undefined;
        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x00], 1)
            .then((data0) => {
                console.assert(data0[0][2] === 0, 'Inconsistent');
                const page0 = data0[0].slice(3);
                var cn = si.tools.arr2big([page0[25], page0[26], page0[27]]);
                if (card.cardNumber != cn) {
                    console.warn('SICard11 Error: SI Card Number inconsistency');
                }

                card.startTime = si.tools.arr2time(page0.slice(14, 16));
                card.finishTime = si.tools.arr2time(page0.slice(18, 20));
                card.checkTime = si.tools.arr2time(page0.slice(10, 12));
                len = Math.min(page0[22] - 1, 128);
                card.punches = new Array(len);

                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x04], 1);
            })
            .then((data4) => {
                console.assert(data4[0][2] === 4, 'Inconsistent');
                const page4 = data4[0].slice(3);
                const is4LastBlock = readSiCard11TimeBlock(page4, card.punches, 0, len);
                if (is4LastBlock) {
                    return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                }
                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x05], 1).then((data5) => {
                    console.assert(data5[0][2] === 5, 'Inconsistent');
                    const page5 = data5[0].slice(3);
                    const is5LastBlock = readSiCard11TimeBlock(page5, card.punches, 1, len);
                    if (is5LastBlock) {
                        return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                    }
                    return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x06], 1).then((data6) => {
                        console.assert(data6[0][2] === 6, 'Inconsistent');
                        const page6 = data6[0].slice(3);
                        const is6LastBlock = readSiCard11TimeBlock(page6, card.punches, 2, len);
                        if (is6LastBlock) {
                            return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                        }
                        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x07], 1).then((data7) => {
                            console.assert(data7[0][2] === 7, 'Inconsistent');
                            const page7 = data7[0].slice(3);
                            readSiCard11TimeBlock(page7, card.punches, 3, len);
                            return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                        });
                    });
                });
            });
    }},
    SIAC: {vals: [8000000, 9000000], description: 'SIAC', read: (card) => {
        const readSIACTimeBlock = (blockData, punchData, blockIndex, punchCount) => {
            let isLastBlock = false;
            const punchesPerBlock = 32;
            for (let i = 0; i < punchesPerBlock; i++) {
                if (blockIndex * punchesPerBlock + i >= punchCount) {
                    isLastBlock = true;
                    break;
                }
                var time = si.tools.arr2time(blockData.slice(i * 4 + 2, i * 4 + 4));
                if (0 <= time) {
                    punchData[blockIndex * punchesPerBlock + i] = {
                        code: blockData[i * 4 + 1],
                        time: time,
                    };
                } else {
                    console.warn('SIAC Error: Undefined Time in punched range');
                }
            }
            return isLastBlock;
        };
        let len = undefined;
        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x00], 1)
            .then((data0) => {
                console.assert(data0[0][2] === 0, 'Inconsistent');
                const page0 = data0[0].slice(3);
                var cn = si.tools.arr2big([page0[25], page0[26], page0[27]]);
                if (card.cardNumber != cn) {
                    console.warn('SIAC Error: SI Card Number inconsistency');
                }

                card.startTime = si.tools.arr2time(page0.slice(14, 16));
                card.finishTime = si.tools.arr2time(page0.slice(18, 20));
                card.checkTime = si.tools.arr2time(page0.slice(10, 12));
                len = Math.min(page0[22] - 1, 128);
                card.punches = new Array(len);

                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x04], 1);
            })
            .then((data4) => {
                console.assert(data4[0][2] === 4, 'Inconsistent');
                const page4 = data4[0].slice(3);
                const is4LastBlock = readSIACTimeBlock(page4, card.punches, 0, len);
                if (is4LastBlock) {
                    return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                }
                return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x05], 1).then((data5) => {
                    console.assert(data5[0][2] === 5, 'Inconsistent');
                    const page5 = data5[0].slice(3);
                    const is5LastBlock = readSIACTimeBlock(page5, card.punches, 1, len);
                    if (is5LastBlock) {
                        return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                    }
                    return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x06], 1).then((data6) => {
                        console.assert(data6[0][2] === 6, 'Inconsistent');
                        const page6 = data6[0].slice(3);
                        const is6LastBlock = readSIACTimeBlock(page6, card.punches, 2, len);
                        if (is6LastBlock) {
                            return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                        }
                        return card.mainStation._sendCommand(si.proto.cmd.GET_SI8, [0x07], 1).then((data7) => {
                            console.assert(data7[0][2] === 7, 'Inconsistent');
                            const page7 = data7[0].slice(3);
                            readSIACTimeBlock(page7, card.punches, 3, len);
                            return card.mainStation._sendCommand(si.proto.ACK, [], 0).then(() => card);
                        });
                    });
                });
            });
    }},
    PCard: {vals: [4000000, 5000000], description: 'pCard', read: (_card) => undefined},
    TCard: {vals: [6000000, 7000000], description: 'tCard', read: (_card) => undefined},
    FCard: {vals: [14000000, 15000000], description: 'fCard', read: (_card) => undefined},
};

SiCard.typeByCardNumber = (cn) => {
    if (!SiCard._typeLookup) {
        SiCard._typeLookup = {borderList: [], borderLookup: {}};
        Object.keys(SiCard.Type).map((k) => {
            var vals = SiCard.Type[k].vals;
            if ((vals.length % 2) != 0) {
                throw new Error(`SiCard.Type.${k}: vals length is ${vals.length}?!? (must be even)`);
            }
            var lastEvenVal = 0;
            for (var i = 0; i < vals.length; i++) {
                var borderList = SiCard._typeLookup.borderList;
                let j;
                for (j = 0; j < borderList.length && borderList[j] < vals[i]; j++) {
                    // TODO: binary search here
                }
                var borderExisted = (SiCard._typeLookup.borderList[j] == vals[i]);
                if (!borderExisted) { SiCard._typeLookup.borderList.splice(j, 0, vals[i]); }
                if ((i % 2) == 0) {
                    let collidingRange;
                    if (borderExisted) {
                        collidingRange = SiCard._typeLookup.borderLookup[vals[i]];
                        if (collidingRange) {
                            throw new Error(`SiCard.Type.${k}: ${vals[i]} would collide with ${collidingRange}`);
                        }
                    }
                    if (!borderExisted && 0 < j) {
                        collidingRange = SiCard._typeLookup.borderLookup[SiCard._typeLookup.borderList[j - 1]];
                        if (collidingRange) {
                            throw new Error(`SiCard.Type.${k}: ${vals[i]} would collide with ${collidingRange}`);
                        }
                    }
                    SiCard._typeLookup.borderLookup[vals[i]] = k;
                    lastEvenVal = vals[i];
                } else {
                    if (lastEvenVal != SiCard._typeLookup.borderList[j - 1]) {
                        throw new Error(`SiCard.Type.${k}: ${vals[i]} is not an immediate follow-up of ${lastEvenVal}`);
                    }
                    if (!SiCard._typeLookup.borderLookup[vals[i]]) { SiCard._typeLookup.borderLookup[vals[i]] = false; }
                }
            }
        });
    }
    let j;
    for (j = 0; j < SiCard._typeLookup.borderList.length && SiCard._typeLookup.borderList[j] <= cn; j++) {
        // TODO: binary search here
    }
    if (j == 0) { return false; }
    return SiCard._typeLookup.borderLookup[SiCard._typeLookup.borderList[j - 1]];
};

si.Card = SiCard;


// ###### General ######

si.onLoad = () => undefined;
window.addEventListener('load', () => {
    SiMainStation.startDeviceDetection();
    if (si.onLoad) { si.onLoad(); }
}, true);
