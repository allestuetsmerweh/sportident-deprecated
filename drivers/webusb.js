/* global si */

const thisDriver = {
    name: 'WebUSB',
    _siConfiguration: 1,
    _siInterface: 0,
    _siAlternate: 0,
    _siEndpoint: 1,
    _siPacketSize: 64,
    siDeviceFilters: [
        {vendorId: 0x10c4, productId: 0x800a},
    ],
    new: () => {
        navigator.usb.requestDevice({
            filters: thisDriver.siDeviceFilters,
        })
            .then((selectedDevice) => {
                if (!(`USB-${selectedDevice.serialNumber}` in si.MainStation.allByDevice)) {
                    new si.MainStation({
                        ident: `USB-${selectedDevice.serialNumber}`,
                        name: selectedDevice.productName,
                        driver: thisDriver,
                        _device: selectedDevice,
                    });
                }
            });
    },
    detect: () => {
        navigator.usb.getDevices().then((devices) => {
            devices.map((device) => {
                const matchesSiDeviceFilter = thisDriver.siDeviceFilters.findIndex((filter) =>
                    device.vendorId === filter.vendorId && device.productId === filter.productId) !== -1;
                if (matchesSiDeviceFilter) {
                    if (!(`USB-${device.serialNumber}` in si.MainStation.allByDevice)) {
                        new si.MainStation({
                            ident: `USB-${device.serialNumber}`,
                            name: device.productName,
                            driver: thisDriver,
                            _device: device,
                        });
                    }
                }
            });
        });
    },
    open: (ms) => new Promise((resolve, _reject) => {
        const dev = ms.device._device;

        ms.device._receiveLoop = () => {
            dev.transferIn(thisDriver._siEndpoint, thisDriver._siPacketSize).then((response) => {
                ms.device._receiveLoop();
                console.debug('Successful receive', response);
                var bufView = new Uint8Array(response.data.buffer);
                ms._logReceive(bufView);
                for (var i = 0; i < bufView.length; i++) { ms._respBuffer.push(bufView[i]); }
                ms._processRespBuffer();
            });
        };

        console.debug('Opening...', ms.device);
        dev.open()
            .then(() => {
                console.debug('Resetting...', dev);
                return dev.reset();
            })
            .then(() => {
                console.debug('Selecting Configuration...', dev);
                return dev.selectConfiguration(thisDriver._siConfiguration);
            })
            .then(() => {
                console.debug('Claiming Interface...', dev);
                return dev.claimInterface(thisDriver._siInterface);
            })
            .then(() => {
                console.debug('Selection Alternate Interface...', dev);
                return dev.selectAlternateInterface(thisDriver._siInterface, thisDriver._siAlternate);
            })
            .then(() => {
                console.debug('Enabling Serial...');
                return dev.controlTransferOut({
                    requestType: 'vendor',
                    recipient: 'interface',
                    request: 0x00,
                    value: 0x01,
                    index: thisDriver._siInterface,
                });
            })
            .then(() => {
                console.debug('Setting Baudrate...');
                return dev.controlTransferOut({
                    requestType: 'vendor',
                    recipient: 'interface',
                    request: 0x1E,
                    value: 0x00,
                    index: thisDriver._siInterface,
                }, new Uint8Array([0x00, 0x96, 0x00, 0x00]).buffer);
            })
            .then(() => {
                console.debug('Starting Receive Loop...');
                ms.device._receiveLoop();
                resolve();
            });
    }),
    close: (ms) => new Promise((resolve, _reject) => {
        const dev = ms.device._device;
        console.debug('Disabling Serial...');
        dev.controlTransferOut({
            requestType: 'vendor',
            recipient: 'interface',
            request: 0x00,
            value: 0x00,
            index: thisDriver._siInterface,
        })
            .then(() => {
                console.debug('Releasing Interface...');
                return dev.releaseInterface(thisDriver._siInterface);
            })
            .then(() => {
                console.debug('Closing Device...');
                return dev.close();
            })
            .then(() => {
                resolve();
            });
    }),
    send: (ms, buffer) => new Promise((resolve, _reject) => {
        const dev = ms.device._device;

        dev.transferOut(thisDriver._siEndpoint, buffer)
            .then(() => {
                console.debug('Successful send');
                resolve();
            });
    }),
};

si.MainStation.drivers.webusb = thisDriver;
